import config.GitlabProperties

class GitlabClient(
    gitlabProperties: GitlabProperties
) {
    // TODO: pass correct parameters
    val redirectPathForAuthenticate = gitlabProperties.apiBaseUrl + "/oauth/authorize?client_id=APP_ID&redirect_uri=REDIRECT_URI&response_type=code&state=YOUR_UNIQUE_STATE_HASH&scope=REQUESTED_SCOPES"
}
