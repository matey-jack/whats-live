package config

class GitlabProperties(
    val apiBaseUrl: String,
    val appId: String
) {
    companion object {
        fun from(source: ConfigurationSource) = GitlabProperties(
            apiBaseUrl = source.getMandatoryString("gitlab_api_base_url"),
            appId = source.getMandatoryString("gitlab_app_id")

        )
    }
}

