package config

import mu.KotlinLogging
import org.tomlj.Toml
import org.tomlj.TomlParseResult
import java.lang.RuntimeException
import java.nio.file.Path

/**
 * This is a mini-library for reading configuration from different sources.
 *
 * Design principle: config variable names should be spelled the same way in each source (file or
 * environment variable) and should not be nested such that a full-text search in source code can
 * always find all the usages. – That's a large benefit for devops and also makes it safe to
 * copy-paste a setting from a configuration file to a Kubernetes config or vice-versa.
 *
 * Note that we still need to pay attention at TOMLs encoding rules for Strings to always have
 * quotes and numbers to never have quotes. This is the same for JSON, and users should do it the
 * same way in YAML, too. (Ignoring that YAML also allows unquoted strings!) I recommend the same
 * even when setting environment variables on the command line and in Docker files: quote your
 * strings, but not your numbers. You need to enforce it yourself and you'll be rewarded by
 * eliminating a whole class of copy-and-paste errors!
 *
 */
class MissingConfigurationException(message: String) : RuntimeException(message)

interface ConfigurationSource {
    fun getMandatoryString(name: String): String = getStringOrNull(name)
        ?: throw MissingConfigurationException("Missing mandatory configuration property '$name'.")

    fun getMandatoryLong(name: String): Long = getLongOrNull(name)
        ?: throw MissingConfigurationException("Missing mandatory configuration property '$name'.")

    fun getStringOrNull(name: String): String?
    fun getLongOrNull(name: String): Long?
}

private val log = KotlinLogging.logger {}

/**
 * We want to enable configuration with environment variables, because that's so convenient in Kubernetes.
 * We also want a configuration file to allow easy local development.
 * I chose TOML as a file format because it properly handles escaping.
 * To avoid confusion between the environment variables and TOML keys, we don't use nesting in the TOML file.
 * We also need to take care to parse numbers in the same way from the environment variables as TOML does.
 * (Reuse or copy-paste the code from jtoml.)
 */
class DefaultConfigurationSource(
    configFile: Path? = null
) : ConfigurationSource {

    val tomlParseResult = configFile?.parseToml()

    private fun getStringFromToml(name: String): String? =
        tomlParseResult?.get(name).let { value ->
            if (value is String) value else {
                val type = if (value == null) "null" else value::class.simpleName
                log.error { "Configuration item '$name' should be a String in TOML file, but is '$type'." }
                null
            }
        }

    private fun getLongFromToml(name: String): Long? =
        tomlParseResult?.get(name).let { value ->
            if (value is Long) value else {
                val type = if (value == null) "null" else value::class.simpleName
                log.error { "Configuration item '$name' should be a Long integer number in TOML file, but is '$type'." }
                null
            }
        }

    private fun getLongFromEnvironment(name: String): Long? =
        System.getenv(name)?.let { stringValue ->
            try {
                stringValue.toLong()
            } catch (e: NumberFormatException) {
                log.error(e) {
                    "Configuration from environment variable '$name' has value '$stringValue' " +
                        "which cannot be parsed to a Long integer number value."
                }
                null
            }
        }

    override fun getStringOrNull(name: String): String? =
        System.getenv(name) ?: getStringFromToml(name)

    override fun getLongOrNull(name: String): Long? =
        getLongFromEnvironment(name) ?: getLongFromToml(name)
}

private fun Path.parseToml(): TomlParseResult =
    Toml.parse(this).also { result ->
        result.errors().forEach {
            log.error { it.toString() }
        }
    }

