import config.DefaultConfigurationSource
import config.GitlabProperties
import io.javalin.Javalin
import java.nio.file.Paths

fun main() {
    val configSource = DefaultConfigurationSource(Paths.get("local-dev-config.yaml"))
    val gitlabClient = GitlabClient(GitlabProperties.from(configSource))

    val app = Javalin.create().start(8080)
    app.get("/") { ctx -> ctx.result("Hello World") }
    app.get("/login") { ctx -> ctx.redirect(gitlabClient.redirectPathForAuthenticate) }
}