plugins {
    val kotlinVersion = "1.3.21"
    id("org.jetbrains.kotlin.jvm") version kotlinVersion
    id("com.github.ben-manes.versions") version "0.29.0"
    id("com.google.cloud.tools.jib") version "2.4.0"

    application
}

application {
    mainClassName = "ServerMain.kt"
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("io.javalin:javalin:2.8.0")
    implementation("org.slf4j:slf4j-simple:1.7.26")
    implementation("io.github.microutils:kotlin-logging:1.8.3")

    implementation("org.tomlj:tomlj:1.0.0")
    implementation("org.gitlab4j:gitlab4j-api:4.15.3")
}

repositories {
    jcenter()
}